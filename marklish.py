#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Started with bits and pieces of https://www.heatonresearch.com/2017/03/03/python-basic-wikipedia-parsing.html
# Using XML parsing memory saving tips from https://stackoverflow.com/questions/24126299/running-out-of-memory-using-python-elementtree
# Markov code from https://eli.thegreenplace.net/2018/elegant-python-code-for-a-markov-chain-text-generator/

#import xml.etree.ElementTree as etree
#import xml.etree.cElementTree as etree
from lxml import etree
import codecs
import csv
import time
import os
import re
from collections import defaultdict, Counter
import pickle
import random
import subprocess


# TODO set locale to avoid formencode error? tweet about it if successful.
from sqlobject import *

class StoredIPA(SQLObject):
  word = StringCol(alternateID=True)
  ipa = StringCol(notNone=True)


def prepare_db():
  sqlhub.processConnection = connectionForURI('sqlite:ipas.db')
  StoredIPA.createTable(ifNotExists=True)


class IPA:
  def __init__(self):
    pass
  
  def extract_ipas(self, source_path):
    print("Extracting...")
    
    def strip_tag_name(t):
      t = elem.tag
      idx = k = t.rfind("}")
      if idx != -1:
        t = t[idx + 1:]
      return str(t)
    
    # We need to keep state when we're streaming the XML
    should_process = False
    in_page = False
    first_ipa = None
    current_word = None
    num_tags_processed = 0
    num_ipas_extracted = 0
    
    for event, elem in etree.iterparse(source_path, events=('start', 'end')):
      tname = strip_tag_name(elem.tag)
      
      num_tags_processed += 1
      if num_tags_processed % 10000 == 0:
        print("Processed {} tags, storing {} IPAs".format(num_tags_processed, num_ipas_extracted))
      
      if event == 'start':
        if tname == 'page':
          # New page, new state
          in_page = True
          first_ipa = None
          current_word = None
      
      if event == "end":
        if tname == "page":
          in_page = False
          if current_word is not None and first_ipa is not None and len(first_ipa) > 2:
            print("IPA for {} is {}".format(current_word, first_ipa[1:-1]))
            try:
              existing_ipa = StoredIPA.byWord(current_word)
            except SQLObjectNotFound:
              existing_ipa = None
            if existing_ipa is None:
              StoredIPA(word = current_word, ipa = first_ipa[1:-1])
            num_ipas_extracted += 1
          # Free memory is possible (Only if using lxml!)
          elem.clear()
          while elem.getprevious() is not None:
            del elem.getparent()[0]    
        if tname == "title":
          #print(u"Processing page with title '{}'".format(elem.text))
          current_word = elem.text.lower()
          should_process = in_page and current_word.find(":") == -1 and current_word.find(" ") == -1
        if tname == "text":
          if not should_process:
            #print("  Ignoring.")
            pass
          else:
            for ipa_match in re.findall(r"{{IPA\|(.*?)\}}", elem.text):
              parts = ipa_match.split("|")
              lang = parts[-1]
              # TODO configurable target language
              if lang == "lang=en":
                ipas = parts[0:-1]
                for ipa in ipas:
                  if first_ipa is None:
                    first_ipa = str(ipa)
                  #print("  IPA: {} (Lang: {})".format(ipa, lang))
    print("Done.")


class IPAIzer:
  def __init__(self):
    pass
  
  def translate(self, text):
    """Convert a string to words, then construct a new string with every word
    replaced by either its IPA representation or nothing"""
    converted = ""
    
    for paragraph in text.split("\n\n"):
      for sentence in paragraph.split("."):
        replacements = []
        for word in sentence.split():
          replacement = None
          try:
            ipa = StoredIPA.byWord(word)
            replacement = ipa.ipa
          except SQLObjectNotFound:
            pass
          if replacement is not None:
            replacements.append(replacement)
        if len(replacements) > 0:
          converted += " ".join(replacements) + ".\n"
      if not converted.endswith("\n\n"):
        converted += "\n"
    return converted


class Markov:
  def __init__(self, state_len):
    self.model = defaultdict(Counter)
    self.state_len = state_len
  
  def analyze(self, source):
    history = ""
    for i in range(len(source) - self.state_len):
      state = source[i:i+self.state_len]
      n = source[i+self.state_len]
      self.model[state][n] += 1
  
  def store(self):
    pickle.dump(self.model, open("markov.model", "wb"))
  
  def load(self):
    self.model = pickle.load(open("markov.model", "rb"))
  
  def generate(self):
    state = random.choice(list(self.model))
    out = list(state)
    for i in range(400):
      selected = random.choices(list(self.model[state]), self.model[state].values())
      out.extend(selected)
      state = state[1:] + selected[0]
    return ''.join(out)


prepare_db()

if __name__ == "__main__":
  
  import sys
  ipas = IPA()
  
  if sys.argv[1] == "extract":
    # Find the IPAs of words from a Wiktionary database dump
    ipas.extract_ipas(sys.argv[2])
  
  if sys.argv[1] == "analyze":
    # Determine sound pattern statistics from some source texts, storing generated model
    ipaizer = IPAIzer()
    ipas = ipaizer.translate(open(sys.argv[2]).read())
    #print(ipas)
    markov = Markov(3)
    markov.analyze(ipas)
    markov.store()
  
  if sys.argv[1] == "generate":
    # Generate a bunch of seemingly reasonable sounds and print IPAs to stdout
    markov = Markov(3)
    markov.load()
    print(markov.generate())
  
  if sys.argv[1] == "speak":
    # Try to vovalize the contents of a file, probably previously generated
    source = open(sys.argv[2]).read()
    for sentence in source.split("."):
      sentence = sentence.strip()
      if len(sentence) == 0:
        continue
      proc = subprocess.run(["./foreign/lexconvert.py", "--phones2phones", "unicode-ipa", "cepstral", sentence], capture_output=True)
      espeak_phonemes = proc.stdout.decode("utf8")
      open("speak_intermediate.txt", "w").write(espeak_phonemes)
      print(espeak_phonemes)
      subprocess.run(["espeak", espeak_phonemes])
  
