# Marklish

## Markov generated spoken English

Use pronounciation data from Wiktionary to determine how words are spoken, then generate
a sequence of IPA syllables from the Wiktionary texts and build a Markov chain using it.
Finally generate something that sounds like English, but is gibberish.


## Steps:

* Download Wiktionary database
* Use marklish.py to extract IPAs for words in English
* Convert some source text into a stream of IPA phonemes
* Markov-process the stream
* Generate new IPAs
* Speak it
 * Using Google:
  * Use https://cloud.google.com/text-to-speech/ to say it
 * Using local programs:
  * Use http://people.ds.cam.ac.uk/ssb22/gradint/lexconvert.html to convert for next step
  * Use http://espeak.sourceforge.net to convert to audio OR use MacOS say - previous step supports both
  * Play audio
